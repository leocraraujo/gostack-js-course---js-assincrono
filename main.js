axios.get('https://api.github.com/users')
    .then(function(response){
        console.log(response);
    })
    .catch(function(error){
        console.log(error);
    })





//Sem a utilização do "AXIOS" teriamos que utilizar uma promisse dessa forma
/*var minhaPromise = function(){
    return new Promise(function(resolve, reject){

        var xhr = new XMLHttpRequest();
        //primeiro o parametro metodo get e depois a url
        xhr.open('GET', 'https://api.github.com/users');
        xhr.send(null);


        xhr.onreadystatechange = function(){
            if(xhr.readyState === 4){
                if(xhr.status === 200){
                    resolve(JSON.parse(xhr.responseText));
                } else {
                    reject('Erro na requisição');
                }
            }
        }
    });
} 


minhaPromise()
    .then(function(response){
        console.log(response);
    })
    .catch(function(error){
        console.log(error);
    })

*/

 